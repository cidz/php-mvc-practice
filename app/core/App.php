<?php

class App {

    protected $controller = 'home';
    protected $method = 'index';
    protected $param = [];
     
    public function __construct() {
        
       $url = $this->parseUrl();
       
        // Get and Set the controller from the first array value of url   
        // then Unset it to url array
       if (file_exists ('../app/controllers/' . $url[0] . '.php')) {
            $this->controller = $url[0];
            unset($url[0]);
       }

        // require the set controller   
       require_once '../app/controllers/' . $this->controller . '.php';

       $this->controller = new $this->controller;

        // Get the method from the 2nd array value of url   
        // then check if the method is exist in the set controller
        // Set the method value
        // then Unset it to url array
       if (isset($url[1])) {
            if (method_exists ($this->controller, $url[1]) ) {
                $this->method = $url[1];
                unset($url[1]);
            }
       }

        // Set the param value
       $this->param = $url ? array_values ($url) : [];
       
        // execute the class method that sets    
       call_user_func_array( [ $this->controller, $this->method ], $this->param );

    }

    public function parseUrl () {

        if (isset( $_GET['url'])) {
            
            // Parsing the URL to array
            return $url = explode('/',filter_var(rtrim($_GET['url'],'/'), FILTER_SANITIZE_URL));

        }

    }
}